

// Asignación de la inicialización de Javascript en la web
window.onload = inicializar;

/**
 * Función para inicializar la página, las variables y eventos necesarios para 
 * su funcionamiento
 * @returns {undefined}
 */
function inicializar()
{

    // Definimos un nuevo array de ámbito global para albergar las canciones a reproducir
    canciones = new Array();

    // Incluimos las canciones en el array
    canciones.push('audio/Intro.mp3');
    canciones.push('audio/Believe.mp3');
    canciones.push('audio/OPP.mp3');
    canciones.push('audio/Spiritual.mp3');

    // Definimos una variable global que será la encargada de almacenar la posición 
    // que tiene la canción que está cargada en el reproductor
    posicion = 0;

    // Inicializamos una variable para controlar la reproducción aleatoria
    aleatoria = false;

    // Variable para controlar si la canción ha sido forzada a cargarse 
    // haciendo doble click en la lista de reproducción
    forzada = false;

    // Comprobamos usando botón de reproducir los tipos de sistemas 
    // que se pueden usar para enlazar elementos y los almacenamos en una variables 
    // globales para poder usar en la función crearEvento
    tieneEventlistener = (document.getElementById('play').addEventListener ? true : false);
    tieneAttachEvent = (document.getElementById('play') ? true : false);

    // Finalmente asignamos las funciones a los botones correspondientes
    crearEvento(document.getElementById('play'), 'click', play);
    crearEvento(document.getElementById('stop'), 'click', stop);
    crearEvento(document.getElementById('ffd'), 'click', ffd);
    crearEvento(document.getElementById('rwd'), 'click', rwd);

    crearEvento(document.getElementById('volDown'), 'click', volDown);
    crearEvento(document.getElementById('volMute'), 'click', volMute);
    crearEvento(document.getElementById('volUp'), 'click', volUp);

    crearEvento(document.getElementById('shuffle'), 'click', shuffle);

    // Creamos un evento que se ejecutará cuando finalice la reproducción de 
    // una canción
    crearEvento(document.getElementById('player'), 'ended', ffd);

    // Dibujamos la lista de reproducción
    crearListaReproduccion(true);

}


/**
 * Función que se encarga de reproducir el tema seleccionado
 * @returns {undefined}
 */
function play()
{
    // Recuperamos el reproductor
    var reproductor = document.getElementById('player');

    // Verificamos que el repdroductor no tenga ningun tema cargado, que la 
    // reproducción aleatoria esté activada y que no se haya forzado la carga 
    // de un tema específico
    if (aleatoria && reproductor.src === "" && !forzada)
    {
        // En este caso (pulsar el botón de play con la reproducción aleatoria) 
        // generamos un número de forma aleatoria que será el próxima tema a 
        // ejecutarse
        posicion = valorAleatorio(0, canciones.length - 1);
    }

    // Comprobamos si el tema que está seleccionado para reproducirse es el mismo que está actualmente cargado
    if (comprobarReproduccion(reproductor.src, canciones[posicion]))
    {
        // Si no es así, lo cargamos
        reproductor.src = canciones[posicion];
    }

    // Recuperamos datos del fichero MP3 antes de empezar a reproducirlo
    ID3v2.parseURL(canciones[posicion], recuperarCaratula);

    // Cambiamos el valor de la variable de control
    forzada = false;

    // Creamos la lista de reproducción
    crearListaReproduccion(false);


    // Forzamos que el scroll se posicione en la fila de la lista de reproducción 
    // correspondiente al id de la posición de reproducción
    document.getElementById(posicion).scrollIntoView();
    
    // Reproducimos el tema cargado
    reproductor.play();

    
}

/**
 * Función que nos permite reproducir una canción de la lista de reproducción 
 * al hacer doble click sobre ella
 * @param {MouseEvent} e El evento de hacer doble click en la lista de reproducción
 * @returns {undefined}
 */
function playSong(e)
{
    // Recuperamos el elemento sobre el que se ha pulsado
    var fila = e.toElement;

    // Iteramos por los objetos mientras sean objetos de celda de tabla
    while (fila instanceof HTMLTableCellElement)
    {
        // Recuperamos el nodo padre del elemento actual
        fila = fila.parentNode;

        // comprobamos si el elemento es un objeto de fila de tabla
        if (fila instanceof HTMLTableRowElement)
        {
            // Si es así, asignamos como posición el id del elemento
            posicion = parseInt(fila.id);

            // Cambiamos el valor de la variable de control
            forzada = true;

            // Llamamos a la función play para que se encargue de reproducir
            play();
        }
    }


}


/**
 * Función que se encarga de pausar la reproducción del tema
 * @returns {undefined}
 */
function stop()
{
    // Recuperamos el reproductor
    var reproductor = document.getElementById('player');

    // Pausamos la reproducción
    reproductor.pause();

}

/**
 * Función que se encarga de pasar a la siguiente canción y reproducirla
 * @returns {undefined}
 */
function ffd()
{
    // Comprobamos si está activado la reproducción aleatoria
    if (!aleatoria)
    {
        // Comprobamos que el cursor no esté en la última posición del array
        if (posicion < canciones.length - 1) {

            // Si no es así, incrementamos en uno el cursor del tema a reproducir
            posicion++;
        }
    }
    else
    {
        // Si está habilitado generamos una nueva posición aleatoriamente
        posicion = valorAleatorio(0, canciones.length - 1);
    }

    // Y llamamos a la función play para que se encargue de cargarlo y 
    // reproducirlo
    play();
}

/**
 * Función que se encarga de pasar a la canción anterior y reproducirla
 * @returns {undefined}
 */
function rwd()
{
    // Comprobamos si está activado la reproducción aleatoria
    if (!aleatoria)
    {
        // Comprobamos que el cursor no esté en la primera posición del array
        if (posicion > 0)
        {

            // Si no es así, incrementamos en uno el cursor del tema a reproducir
            posicion--;
        }
    }
    else
    {
        // Si está habilitado generamos una nueva posición aleatoriamente
        posicion = valorAleatorio(0, canciones.length - 1);
    }
    // Y llamamos a la función play para que se encargue de cargarlo y 
    // reproducirlo        
    play();
}

/**
 * Función que nos permite aumentar el volumen de reproducción
 * @returns {undefined}
 */
function volUp()
{
    // Recuperamos el reproductor
    var reproductor = document.getElementById('player');

    // Comprobamos si el reproductor está silenciado
    if (reproductor.muted)
    {
        // Si lo está, habilitamos el sonido
        volMute();
    }

    try
    {
        // Aumentamos un poco el volumen
        reproductor.volume += 0.1;
    }
    catch (e)
    {
        // Si se produce algún error al intentar subir el volumen por encima 
        // del rango que le corresponde, capturamos la excepción y ajustamos 
        // el mismo al máximo posible
        reproductor.volume = 1;
    }

}

/**
 * Función que nos permite decrementar el volumen de reproducción
 * @returns {undefined}
 */
function volDown()
{

    // Recuperamos el reproductor
    var reproductor = document.getElementById('player');

    // Comprobamos si el reproductor está silenciado
    if (reproductor.muted)
    {
        // Si lo está, habilitamos el sonido
        volMute();
    }

    try
    {
        // Reducimos un poco el volumen
        reproductor.volume -= 0.1;
    }
    catch (e)
    {
        // Si se produce algún error al intentar bajar el volumen por debajo
        // del rango que le corresponde, capturamos la excepción y ajustamos 
        // el mismo al minimo posible        
        reproductor.volume = 0;
    }
}

/**
 * Función que nos permite silenciar el volumen de reproducción
 * @returns {undefined}
 */
function volMute()
{
    // Recuperamos el reproductor
    var reproductor = document.getElementById('player');

    // Comprobamos si se ha silenciado el sonido
    if (reproductor.muted)
    {
        // Si se ha silenciado, lo habilitamos
        reproductor.muted = false;
    }
    else
    {
        // Si está habilitado, lo silenciamos
        reproductor.muted = true;
    }

}

/**
 * Función que permite activar y desactivar la reproducción aleatoria
 * @returns {undefined}
 */
function shuffle()
{
    // Localizamos el botón de reproducción aleatoria
    var boton = document.getElementById('shuffle');

    // Si la reproducción aleatoria no está activada
    if (!aleatoria)
    {
        // La activamos
        aleatoria = true;

        // Cambiamos la imagen del botón
        boton.src = 'iconos/shuffle.png';
    }
    else
    {
        // En caso contrario la desactivamos
        aleatoria = false;

        // Cambiamos la imagen del botón
        boton.src = 'iconos/noshuffle.png';

    }
}

/**
 * Permite generar un numero entero aleatorio entre dos valores de un rango
 * @param {int} min El valor mínimo que puede generarse
 * @param {int} max El valor máximo que puede generarse
 * @returns {int} Un número entero entre los dos rangos especificados
 */
function valorAleatorio(min, max)
{

    // Asignamos a la variable de salida la posición actual
    var siguiente = posicion;

    // Iteramos mientras las dos variables tengan los mismos valores
    while (siguiente === posicion)
    {
        // Generamos un número aleatoriao entre los rangos especificados
        siguiente = Math.floor(min + Math.random() * (max + 1 - min));
    }

    // Devolvemos el valor generado
    return siguiente;
}


/**
 * Función que permite limpiar el título de los temas a reproducir
 * @param {String} strTitulo
 * @returns {String} Una cadena de texto con el título
 */
function limpiarTitulo(strTitulo)
{
    // Localizamos la última posición del caracter /
    var pos = strTitulo.lastIndexOf("/");

    // Recortamos la cadena desde la posición del último / más 1 hasta el final de la misma
    strTitulo = strTitulo.substr(pos + 1, strTitulo.length);

    // Devolvemos el resultado
    return strTitulo;
}


/**
 * Función que nos permite comprobar si una canción ya se está ejecutando
 * @param {type} temaReproducido El tema que se está reproduciendo actualemnte
 * @param {type} temaReproducir El tema que queremos reproducir
 * @returns {Boolean} True si los temas son distintos, False en caso contrario
 */
function comprobarReproduccion(temaReproducido, temaReproducir)
{
    // Buscamos en la cadena compuesta por el tema que se está reproduciendo, 
    // la cadena que corresponde a el tema a reproducir
    var n = temaReproducido.search(temaReproducir);

    // Si no se encuentra
    if (n === -1)
    {
        // El tema a reproducir no es el mismo y devolvemos true
        return true;
    }
    else
    {
        // En caso contrario, el tema es el mismo y devolvemos false
        return false;
    }
}

/**
 * Función que se encarga de crear una lista de reproducción con los temas a escuchar
 * @param {bool} noSel Valor que nos permite crear la tabla sin seleccionar ningun elemento
 * @returns {undefined}
 */
function crearListaReproduccion(noSel)
{

    // Localizamos el contenedor de la tabla
    var contenedor = document.getElementById('anclaje');

    // Comprobamos si tiene algún hijo
    while (contenedor.childElementCount > 0)
    {
        // De ser así, lo eliminamos
        contenedor.removeChild(contenedor.childNodes[0]);
    }

    // Creamos un nuevo objeto table
    var tabla = document.createElement('table');

    // Le asignamos un id
    tabla.id = 'lista_reproduccion';

    // Creamos una fila para la descripción de las columnas
    var tr = document.createElement('tr');

    // Creamos dos columnas de cabecera
    var th1 = document.createElement('th');
    var th2 = document.createElement('th');

    // Les asignamos una clase para usar en los estilos
    th1.className = 'cabecera';
    th2.className = 'cabecera';


    // Creamos los textos para las cabeceras
    var header1 = document.createTextNode('Número');
    var header2 = document.createTextNode('Nombre');


    // Anexamos el texto a las cabeceras
    th1.appendChild(header1);
    th2.appendChild(header2);

    // Y las cabeceras a la fila
    tr.appendChild(th1);
    tr.appendChild(th2);

    // Finalmente agregamos la fila con la descripción de las columnas a la tabla
    tabla.appendChild(tr);

    // Iteramos tantas veces como elementos tenga el array de partidos que se nos ha pasado como parámetro
    for (var i = 0; i < canciones.length; i++) {

        // Creamos una fila nueva para el politico
        tr = document.createElement('tr');

        // Asignamos como ide de la fila el contador del bucle, el cual asignará 
        // el mismo id que la posición de las canciones en el array
        tr.id = i;

        // Ponemos un tooltip
        tr.title = "Haga doble click para reproducir la canción";

        // Si la función no se ha cargado para que no se seleccione nada y la 
        // posición del puntero de canciones es igual al contador del bucle eso 
        // quiere decir que estamos dibujando la fila que corresponde a la 
        // canción que se está reproduciendo
        if (!noSel && posicion === i)
        {
            // Si es así, asingamos la clase seleccionado para cambiar 
            // el color de fondo de la fila
            tr.className = 'seleccionado';
        }

        // Creamos un evento para que se ejecute la funcion playSong cuando 
        // se haga doble click en la fila
        crearEvento(tr, 'dblclick', playSong);

        // Y dos columnas
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');

        // Recuperamos el valor del número, el nombre de la canción y creamos dos objetos TextNode
        var text1 = document.createTextNode((i + 1));
        var text2 = document.createTextNode(limpiarTitulo(canciones[i]));


        // Añadimos los textos a las columnas
        td1.appendChild(text1);
        td2.appendChild(text2);

        // Las columnas a la fila
        tr.appendChild(td1);
        tr.appendChild(td2);


        // Y la fila a la tabla
        tabla.appendChild(tr);
    }

    // Finalmente, agregamos la tabla creada dinamicamente al contenedor
    document.getElementById('anclaje').appendChild(tabla);
}


/**
 * Función que nos permite crear un evento y asociarlo a una función
 * @param {document.element} elemento Elemento sobre el que se asociará el evento
 * @param {string} tipoEvento Tipo de evento a crear
 * @param {function} funcion Función asociada al evento
 * @returns {undefined}
 */
function  crearEvento(elemento, tipoEvento, funcion)
{
    // Comprobamos si al elemento se le puede añadir un EventListener, lo que 
    // indica que el navegador es compatible con los standards W3C
    if (tieneEventlistener)
    {
        // Asignamos el evento
        elemento.addEventListener(tipoEvento, funcion, false);
    }
    // De no ser así, comprobamos si se le puede adjuntar el un evento, lo que 
    // indica que el navegador es compatible con internet explorer
    else if (tieneAttachEvent)
    {
        // Puesto que al usar attachEvent perdemos la capacidad de acceder al 
        // objeto this dentro de la función que asignamos al evento, usamos el 
        // método call() para pasar el elemento como parámetro y asignarlo al 
        // objeto this de la función
        elemento.attachEvent("on" + tipoEvento, function () {
            funcion.call(elemento);
        });
    }
    // Finalmente si ninguno de los dos métodos anteriores funcionase, usamos 
    // el método tradicional para asignar eventos
    else
    {
        // Asignamos el evento por el método clásico
        elemento["on" + tipoEvento] = funcion;
    }
}


/**
 * Función callback que se llama para recuperar la carátula de la canción
 * @param {Object} tags Objeto que contiene la información ID3 de la canción a reproducir
 * @returns {undefined}
 */
function recuperarCaratula(tags) {


    // Comprobamos si el mp3 contiene carátula
     if (tags.pictures.length > 0)
    {
        // Si es así, la asignamos como imágen de la canción
        document.getElementById('cover').setAttribute('src', tags.pictures[0].dataURL);
    }
    else
    {
        // En caso contrario, cargamos una imágen por defecto
        document.getElementById('cover').src = 'iconos/no-cover';
    }      
    
    // Creamos uan cadena con la información importante de la reproducción
    var valor = tags.Title + " - " +  tags.Artist +" - " + tags.Album;
    
    // La asignamos a la etiqueta creada para hacer de marquesina
    document.getElementsByClassName('marquee')[0].innerHTML = valor;
    
    
}